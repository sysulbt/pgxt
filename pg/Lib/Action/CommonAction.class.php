<?php

/**
 * Created by PhpStorm.
 * User: Jingbo Zhan
 * Date: 2015/4/23
 * Time: 13:51
 */
class CommonAction extends Action
{
    public function _initialize()
    {
        if (!isset($_SESSION['username'])) {
            $this->redirect('/Admin/index');
        }
    }
} 