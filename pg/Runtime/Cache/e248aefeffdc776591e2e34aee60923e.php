<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <style>
        .center {
            text-align: center;
        }
        *{
            font-size: 24px;
        }
    </style>
    <link href="__PUBLIC__/css/bootstrap.min.css" rel="stylesheet">
    <script src="__PUBLIC__/js/jquery-1.11.2.min.js"></script>
    <script src="__PUBLIC__/js/bootstrap.min.js"></script>
</head>
<body>
<div class="row">
    <div class="col-md-8 col-md-offset-2 center">
        <h1>详情展示</h1>
    </div>
    <div class="col-md-3 col-md-offset-2">
        楼盘名称：<?php echo ($info["name"]); ?>
    </div>
    <div class="col-md-3">
         类型：<?php echo ($info["type"]); ?>
    </div>
    <div class="col-md-3">
        楼层：<?php echo ($info["floor"]); ?>
    </div>
    <div class="col-md-3 col-md-offset-2">
        区域：<?php echo ($info["area"]); ?>
    </div>
    <div class="col-md-6 ">
        坐落：<?php echo ($info["position"]); ?>
    </div>
    <div class="col-md-3 col-md-offset-2">
        单价：<?php echo ($info["price"]); ?>元/平方米
    </div>
    <div class="col-md-6 ">
        创建时间：<?php echo ($info["date"]); ?>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-md-offset-2 col-md-8">
        <?php echo ($info["content"]); ?>
    </div>
</div>
</body>
</html>