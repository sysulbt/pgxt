<?php
/**
 * Created by PhpStorm.
 * User: Jingbo Zhan
 * Date: 2015/4/23
 * Time: 19:36
 */

class AdminAction extends  Action{
    public function index(){
        $this->display();
    }
    public function login(){
        $username = I('username');
        $password = I('password',"",'md5');
        $count =count(M('user')->where(array('username'=>$username,'password'=>$password))->select());

        if($count != 0){
            session('username',$username);
            $this->redirect("Information/infoList");
        }else{
            $this->error('用户名或密码错误，请重试');
        }
    }

} 