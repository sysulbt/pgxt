<?php

/**
 * Created by PhpStorm.
 * User: Jingbo Zhan
 * Date: 2015/4/21
 * Time: 18:22
 */
class QueryAction extends Action
{
    public function index()
    {
        $this->pageNumber = 1;
        $this->display();
    }
    public function getlist(){
        $page = I('page',1,'intval');
        $area = I('area','');
        $name = I('name','');
        $position = I('position','');
        $type = I('type','');
        $where_arr = array();
        if($area != ''){
            $where_arr['area'] = array('eq',$area);
        }
        if($name != ''){
            $where_arr['name'] = array('like','%'.$name.'%');
        }
        if($position != ''){
            $where_arr['position'] = array('like','%'.$position.'%');
        }
        if($type != ''){
            $where_arr['type'] = array('eq',$type);
        }
        $list = M('information')->where($where_arr)->limit(($page-1)*C('LIST_PRE_PAGE').','.C('LIST_PRE_PAGE'))->select();
        echo json_encode($list);
    }
    public function content(){
        $list =M('information')->where(array('id'=>I('id',0,'intval')))->select();
        $this->info= $list[0];
        $this->display();
    }
}