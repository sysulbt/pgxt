<?php

/**
 * Created by PhpStorm.
 * User: Jingbo Zhan
 * Date: 2015/4/22
 * Time: 15:39
 */
class InformationAction extends CommonAction
{
    public function  index()
    {
        $this->display();
    }

    public function addinfo()
    {
        $this->display();
    }

    public function addHandle()
    {
        if (!IS_POST) {
            $this->error("请用正常方式访问", 5, U('/Information/add'));
        } else {
            if (M('information')->add($_POST)) {
                $this->success("添加成功",U("/Information/infoList"));
            } else {
                $this->error("添加失败");
            }
        }
    }

    public function edit(){
        $info = M("information")->where(array('id'=>I('id')))->select();
        if(count($info)){
            $this->assign('info',$info[0]);
            $this->display();
        }else{
            $this->error("系统错误");
        }

    }
    public function editHandle(){
        if(!IS_POST){
            $this->error("请用正常方式访问");
        }else{
            if (M('information')->save($_POST)) {
                $this->success("修改成功",U("/Information/infoList"));
            } else {
                $this->error("修改失败");
            }
        }
    }
    public function del(){
        $id = I('id',array());
        if(count($id) > 0){
            $flag = M('information')->where(array('id'=>array('in',$id)))->delete();
        }
        if($flag){
            $this->success('删除成功',U("/Information/infolist"));
        }else{
            $this->error('删除失败');
        }
    }

    public function infoList(){
        $this->pageNumber = 1;
        $this->display();
    }
} 