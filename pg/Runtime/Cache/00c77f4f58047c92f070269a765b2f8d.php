<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>评估信息汇总系统-编辑</title>
    <link href="__PUBLIC__/css/bootstrap.min.css" rel="stylesheet">
    <link href="__PUBLIC__/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">评估信息汇总系统</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false">信息管理<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">添加信息</a></li>
                        <li><a href="#">信息列表</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false">用户管理<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">添加用户</a></li>
                        <li><a href="#">用户列表</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">欢迎XXX，使用本系统</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>编辑信息</h1>
        </div>
        <div>
            <form action="<?php echo U('/Information/editHandle');?>" method="post">
                <input type="hidden" name="id" value="<?php echo ($info["id"]); ?>">
                <div class="form-group">
                    <label for="name">楼盘名称</label>
                    <input type="text" class="form-control" id="name" name="name" value="<?php echo ($info["name"]); ?>">
                </div>
                <div class="form-group">
                    <label for="position">坐落</label>
                    <input type="text" class="form-control" id="position" name="position" value="<?php echo ($info["position"]); ?>">
                </div>
                <div class="row">
                    <div class="form-group col-md-2">
                        <label for="area">区域</label>
                        <select id="area" class="form-control" name="area">
                            <option value="<?php echo ($info["area"]); ?>"><?php echo ($info["area"]); ?></option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="type">类型</label>
                        <select id="type" class="form-control" name="type">
                            <option value="<?php echo ($info["type"]); ?>"><?php echo ($info["type"]); ?></option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="floor">楼层</label>
                        <input type="text" class="form-control" id="floor" name="floor" value="<?php echo ($info["floor"]); ?>">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="price">单价</label>

                        <div class="input-group">
                            <input type="text" class="form-control" id="price" name="price" placeholder="请填写数值" value="<?php echo ($info["price"]); ?>">
                            <div class="input-group-addon">元/平方米</div>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="date">添加时间</label>
                        <input type="text" class="form-control" id="date" name="date" value="<?php echo ($info["date"]); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="content">内容详情</label>
                    <textarea id="content" name="content" style="height: 300px;">
                        <?php echo ($info["content"]); ?>
                    </textarea>
                </div>
                <button type="submit" class="btn btn-default">修改</button>
            </form>
        </div>
    </div>

</div>
<script src="__PUBLIC__/js/jquery-1.11.2.min.js"></script>
<script src="__PUBLIC__/js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8" src="__PUBLIC__/ue/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="__PUBLIC__/ue/ueditor.all.min.js"></script>
<script type="text/javascript" charset="utf-8" src="__PUBLIC__/ue/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript" src="__PUBLIC__/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="__PUBLIC__/js/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script>
    var ue = UE.getEditor('content');

    $(document).ready(function () {
        $.ajax({
            url: '/pg/select.json',
            type: 'post',
            dataType: 'json',
            success: function (msg) {
                $.each(msg.area, function (index, val) {
                    $('#area').append("<option value='" + val + "'>" + val + "</option>");
                });
                $.each(msg.type, function (index, val) {
                    $('#type').append("<option value='" + val + "'>" + val + "</option>");
                });
            }
        });
    });
    $("#date").datetimepicker({
        format: 'yyyy-mm-dd',
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
</script>


</body>
</html>