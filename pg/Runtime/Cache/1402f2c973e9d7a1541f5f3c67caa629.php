<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>评估信息汇总系统</title>

    <!-- Bootstrap -->
    <link href="__PUBLIC__/css/bootstrap.min.css" rel="stylesheet">
    <script src="__PUBLIC__/js/jquery-1.11.2.min.js"></script>
    <script src="__PUBLIC__/js/bootstrap.min.js"></script>
    <style>
        div {
            text-align: center;
        }

        body {
        }

        table {
            border: black;
        }
    </style>
</head>
<body>
<input id="pageNumber" type="hidden" value="<?php echo ($pageNumber); ?>">

<div class="page">
    <div class="row">
        <div class="col-md-12">
            <h1>评估信息汇总系统</h1>
        </div>
    </div>
    <br/>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form class="form-inline" id="form">
                <div class="form-group">
                    <label for="area">区域:</label>
                    <select class="form-control" id="area" name="area">
                        <option value="">不限制</option>
                    </select>

                </div>
                <div class="form-group">
                    <label for="name">楼盘名称:</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="position">坐落:</label>
                    <input class="form-control" id="position" name="position" type="position">
                </div>
                <div class="form-group">
                    <label for="type">类别：</label>
                    <select class="form-control" id="type" name="type">
                        <option value="">不限制</option>
                    </select>
                </div>
                <div class="form-group">
                    <a id='submit' class="btn btn-default form-control">查询</a>
                </div>
            </form>
        </div>
    </div>
    <br/>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <table class="table table-hover table-bordered table-bordered">
                <thead>
                <tr>
                    <td>序号</td>
                    <td>区域</td>
                    <td>楼盘名称</td>
                    <td>坐落</td>
                    <td>类别</td>
                    <td>楼层</td>
                    <td>单价（元/平方米）</td>
                    <td>录入时间</td>
                    <td>操作</td>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <nav>
            <ul class="pagination">
                <li>
                    <a href="#" id="Previous">
                        上一页
                    </a>
                </li>
                <li>
                    <a href="#" id="Next">
                        下一页
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
</body>
<script>
    $(document).ready(function () {
        $.ajax({
            url: '__ROOT__/select.json',
            type: 'post',
            dataType: 'json',
            success: function (msg) {
                $.each(msg.area, function (index, val) {
                    $('#area').append("<option value='" + val + "'>" + val + "</option>");
                });
                $.each(msg.type, function (index, val) {
                    $('#type').append("<option value='" + val + "'>" + val + "</option>");
                });
            }
        });
        $.ajax({
            url: "<?php echo U('/Query/getlist');?>",
            type: 'post',
            dataType: 'json',
            success: function (data) {
                changeTable(data);
            }

        });
    });
    function changeTable(data) {
        $('tbody').empty();
        str = "";
        href = '<?php echo U("/Query/detail");?>';
        $.each(data, function (index, val) {
            str = str + "<tr>";
            str = str + '<td>' + val.id + '</td>';
            str = str + '<td>' + val.area + '</td>';
            str = str + '<td>' + val.name + '</td>';
            str = str + '<td>' + val.position + '</td>';
            str = str + '<td>' + val.type + '</td>';
            str = str + '<td>' + val.floor + '</td>';
            str = str + '<td>' + val.price + '</td>';
            str = str + '<td>' + val.date + '</td>';
            str = str + '<td>' + '<a href="' + href + "/id/" + val.id + '" class="btn btn-info">查看</a>' + '</td>';
            str = str + '</tr>';

        });
        $('tbody').append(str);
    }
    function getdata() {
        str = $("#form .form-control").map(function () {
            return ($(this).attr("name") + '=' + $(this).val());
        }).get().join("&");
        return str;
    }

    function check(data) {
        return (data != null);
    }
    $('#submit').click(function () {
        $.ajax({
            url: "<?php echo U('/Query/getlist');?>",
            type: 'post',
            data: getdata(),
            dataType: 'json',
            success: function (data) {
                if(check(data)){
                    changeTable(data);
                }else{
                    alert('没有新数据');
                }

            }
        });
    });
    $('#Next').click(function () {
        var page = parseInt($("#pageNumber").val()) + 1;
        $.ajax({
            url: "<?php echo U('/Query/getlist');?>",
            type: 'post',
            data: getdata() + "&page=" + page,
            dataType: 'json',
            success: function (data) {
                if(check(data)){
                    changeTable(data);
                    $("#pageNumber").val(page);
                }else{
                    alert("没有新数据");
                }

            }
        });
    });
    $('#Previous').click(function () {
        var page = parseInt($("#pageNumber").val()) - 1;
        if (page > 0) {
            $.ajax({
                url: "<?php echo U('/Query/getlist');?>",
                type: 'post',
                data: getdata() + "&page=" + page,
                dataType: 'json',
                success: function (data) {
                    if(check(data)){
                        changeTable(data);
                        $("#pageNumber").val(page);
                    }else{
                        alert("没有新数据");
                    }
                }
            });
        }
    });
</script>

</html>